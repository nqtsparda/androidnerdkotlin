package com.example.tungnq5.androidnerdkotlin


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.ListFragment
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ListView
import android.widget.TextView
import com.example.tungnq5.nerdkotlin.Crime
import com.example.tungnq5.nerdkotlin.CrimeFragment


/**
 * A simple [Fragment] subclass.
 */
class CrimeListFragment : ListFragment() {

    var crimes: ArrayList<Crime>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity.setTitle(R.string.crime_title)
        crimes = CrimeLab.crimes
        val crAdapter: CrimeAdapter = CrimeAdapter(activity, R.layout.list_item_crime, crimes!!)
        listAdapter = crAdapter
    }

    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        val selectedCrime: Crime = listAdapter.getItem(position) as Crime
        val intent:Intent = Intent(activity, CrimePagerActivity::class.java)
        intent.putExtra(CrimeFragment.EXTRA_CRIME_ID, selectedCrime.id)
        startActivity(intent)
    }

    private inner class CrimeAdapter(context: Context, layout: Int, list: ArrayList<Crime>) :
            ArrayAdapter<Crime>(context, layout, list) {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var retView: View
            if (convertView == null) {
                retView = activity.layoutInflater.inflate(R.layout.list_item_crime, null)

            } else {
                retView = convertView
            }
            val crime: Crime = getItem(position)
            val tvTitle: TextView = retView.findViewById(R.id.crime_list_item_titleTextView) as TextView
            tvTitle.text = crime.title
            val tvDate = retView.findViewById(R.id.crime_list_item_dateTextView) as TextView
            tvDate.text = crime.date.toString()
            val cbSolved = retView.findViewById(R.id.crime_list_item_solvedCheckBox) as CheckBox
            cbSolved.isChecked = (crime.solved)
            return retView
        }
    }

    override fun onResume() {
        super.onResume()
        (listAdapter as CrimeAdapter).notifyDataSetChanged()
    }

}
