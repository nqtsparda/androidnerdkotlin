package com.example.tungnq5.nerdkotlin

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import com.example.tungnq5.androidnerdkotlin.CrimeLab
import com.example.tungnq5.androidnerdkotlin.R
import java.text.SimpleDateFormat
import java.util.*

class CrimeFragment : Fragment() {

    private var mCrime: Crime? = null
    private var mTitleField: EditText? = null
    private var dateButton: Button? = null
    private var solvedCheckBox: CheckBox? = null

    companion object {
        const val EXTRA_CRIME_ID = "EXTRA_CRIME_ID"
        fun newInstance(crimeId:UUID) : CrimeFragment {
            val arg : Bundle = Bundle()
            arg.putSerializable(EXTRA_CRIME_ID, crimeId)
            val crimeFragment:CrimeFragment = CrimeFragment()
            crimeFragment.arguments = arg
            return crimeFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val crimeId = arguments.getSerializable(EXTRA_CRIME_ID) as UUID
        mCrime = CrimeLab.getCrime(crimeId)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_crime, container, false)
        mTitleField = view.findViewById(R.id.crime_title) as EditText
        mTitleField!!.setText(mCrime!!.title)
        solvedCheckBox = view.findViewById(R.id.crime_solve) as CheckBox

        solvedCheckBox!!.isChecked = mCrime!!.solved
        mTitleField!!.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                    c: CharSequence, start: Int, before: Int, count: Int) {
                mCrime!!.title = c.toString()
            }

            override fun beforeTextChanged(
                    c: CharSequence, start: Int, count: Int, after: Int) {
                // This space intentionally left blank
            }

            override fun afterTextChanged(c: Editable) {
                // This one too
            }
        })

        dateButton = view.findViewById(R.id.crime_date) as Button?
        dateButton!!.isEnabled = false
        val formatDate = SimpleDateFormat("E, MMM dd, yyyy").format(mCrime!!.date)
        dateButton!!.text = formatDate


        solvedCheckBox = view.findViewById(R.id.crime_solve) as CheckBox?
        solvedCheckBox!!.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                solvedCheckBox!!.isChecked = true
            }
        }

        return view
    }

}
