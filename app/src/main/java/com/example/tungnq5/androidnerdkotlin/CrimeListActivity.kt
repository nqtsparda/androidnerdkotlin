package com.example.tungnq5.androidnerdkotlin

import android.support.v4.app.Fragment

/**
 * Created by TungNQ5 on 6/13/2017.
 */
class CrimeListActivity : SingleFragmentActivity() {
    override fun createFragment(): Fragment? {
        return CrimeListFragment()
    }
}