package com.example.tungnq5.androidnerdkotlin

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import com.example.tungnq5.nerdkotlin.Crime
import com.example.tungnq5.nerdkotlin.CrimeFragment
import java.util.*

/**
 * Created by TungNQ5 on 6/20/2017.
 */
class CrimePagerActivity : AppCompatActivity() {

    var viewpager: ViewPager? = null
    var crimeList: ArrayList<Crime>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewpager = ViewPager(this)
        viewpager!!.id = R.id.viewPager
        setContentView(viewpager)

        crimeList = CrimeLab.crimes

        val fm: FragmentManager = supportFragmentManager
        viewpager!!.adapter = object : FragmentStatePagerAdapter(fm) {
            override fun getItem(p0: Int): Fragment {
                val crime: Crime = crimeList!![p0]
                return CrimeFragment.newInstance(crime.id)
            }

            override fun getCount(): Int {
                return crimeList!!.size
            }
        }

        viewpager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {
                val crimeItem:Crime = crimeList!![p0]
                title = crimeItem.title
            }

        })
        val intentAct = intent
        val crimeId = intentAct.getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID) as UUID
        val crime = CrimeLab.getCrime(crimeId) as Crime
        val crimeIndex = crimeList!!.indexOf(crime)
        viewpager!!.setCurrentItem(crimeIndex, false)


    }
}