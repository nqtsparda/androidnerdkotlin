package com.example.tungnq5.androidnerdkotlin

import android.content.Context
import com.example.tungnq5.nerdkotlin.Crime
import java.util.*

/**
 * Created by TungNQ5 on 6/8/2017.
 */
object CrimeLab {
    var crimes: ArrayList<Crime> = ArrayList<Crime>()
    var context: Context? = null

    init {
        for (i in 1..100){
            val c:Crime = Crime()
            c.title = "Crime #" + i
            c.solved = i %2 == 0
            crimes.add(c)
        }
    }

    fun get(c: Context) {
        context = c.applicationContext
    }

    fun getCrime(id: UUID): Crime? {
        for (crime in crimes) {
            if (crime.id == id) {
                return crime
            }
        }
        return null
    }

}