package com.example.tungnq5.androidnerdkotlin

import android.content.Intent
import android.support.v4.app.Fragment
import com.example.tungnq5.nerdkotlin.CrimeFragment
import java.util.*

class CrimeActivity : SingleFragmentActivity() {
    override fun createFragment(): Fragment? {
        val crimeId = intent.getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID) as UUID
        return CrimeFragment.newInstance(crimeId)
    }
}
