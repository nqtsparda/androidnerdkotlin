package com.example.tungnq5.androidnerdkotlin

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager

abstract class SingleFragmentActivity : FragmentActivity() {

    open abstract fun createFragment(): Fragment?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)
        val fm: FragmentManager = supportFragmentManager
        var fragmentCrime: Fragment? = fm.findFragmentById(R.id.fragmentContainer)
        if (fragmentCrime == null) {
            fragmentCrime = createFragment()
            fm.beginTransaction().add(R.id.fragmentContainer, fragmentCrime).commit()
        }
    }

}
